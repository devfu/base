require Rails.root.join *%w[ config heroku ]

Rails.application.configure do
  config.action_controller.perform_caching         = true
  config.action_mailer.perform_caching             = false
  config.active_job.queue_adapter                  = :sidekiq
  config.active_record.dump_schema_after_migration = false
  config.active_support.deprecation                = :notify
  config.assets.cache_store                        = :dalli_store
  config.assets.compile                            = false
  config.assets.compress                           = true
  config.assets.js_compressor                      = :uglifier
  config.cache_classes                             = true
  config.consider_all_requests_local               = false
  config.eager_load                                = true
  config.force_ssl                                 = true
  config.i18n.fallbacks                            = true
  config.log_formatter                             = ::Logger::Formatter.new
  config.log_level                                 = :debug
  config.log_tags                                  = [ :request_id ]
  config.public_file_server.enabled                = ENV['RAILS_SERVE_STATIC_FILES'].present?
  config.public_file_server.headers                = { 'Cache-Control' => 'public, max-age=31536000' }

  config.action_mailer.default_url_options = {
    host: ENV['HEROKU_HOST'] || ENV['EMAIL_HOST'] || ENV['CANONICAL_HOST']
  }

  config.action_mailer.smtp_settings = {
    :address        => 'smtp.sendgrid.net',
    :port           => '25',
    :authentication => :plain,
    :user_name      => ENV['SENDGRID_USERNAME'],
    :password       => ENV['SENDGRID_PASSWORD'],
    :domain         => ENV['SENDGRID_DOMAIN']
  }
end
