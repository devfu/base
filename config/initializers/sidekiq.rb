require 'sidekiq'

Sidekiq.configure_client do |config|
  config.redis = { size: ENV['REDIS_CLIENT_SIZE'] || 2 }
end
