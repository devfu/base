require_relative 'boot'

require 'rails/all'

Bundler.require *Rails.groups

module DevFuBase
  class Application < Rails::Application

    config.active_support.escape_html_entities_in_json = true
    config.responders.flash_keys                       = %i[ success danger ]
    config.sass.preferred_syntax                       = :sass
    config.time_zone                                   = 'Arizona'

  end
end
