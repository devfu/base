class MigrateUserIdToUuid < ActiveRecord::Migration

  def up
    remove_column :users, :id
    rename_column :users, :uuid, :id
    execute 'ALTER TABLE users ADD PRIMARY KEY (id);'
  end

end
