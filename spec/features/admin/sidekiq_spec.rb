require 'spec_helper'

describe 'Sidekiq' do

  subject { page }

  let(:admin) { FactoryGirl.create :admin }
  let(:user)  { FactoryGirl.create :user }

  describe 'dashboard' do

    let(:path) { sidekiq_web_path }

    context 'as a guest' do

      it 'does not define the route' do
        ->{ visit sidekiq_web_path }.should raise_exception ActionController::RoutingError
      end

    end

    context 'as a user' do

      before do
        login_as user
      end

      it 'does not define the route' do
        ->{ visit sidekiq_web_path }.should raise_exception ActionController::RoutingError
      end

    end

    it { should allow_access.to :admin }

  end

end
