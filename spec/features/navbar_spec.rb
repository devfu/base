require 'spec_helper'

describe 'Navbar' do

  subject { page }

  let(:admin) { FactoryGirl.create :admin }
  let(:user)  { FactoryGirl.create :user }

  context 'as a guest' do

    before do
      visit root_path
    end

    it { should_not have_css ".navbar a[href='#{ destroy_user_session_path }'][data-method='delete']" }
    it { should_not have_css ".navbar a[href='#{ edit_user_registration_path }']" }
    it { should_not have_css ".navbar a[href='#{ sidekiq_web_path }']" }

    it { should have_css ".navbar a[href='#{ new_user_session_path }']" }

  end

  context 'as a user' do

    before do
      login_as user
    end

    it { should_not have_css ".navbar a[href='#{ new_user_session_path }']" }
    it { should_not have_css ".navbar a[href='#{ sidekiq_web_path }']" }

    it { should have_css ".navbar a[href='#{ destroy_user_session_path }'][data-method='delete']" }
    it { should have_css ".navbar a[href='#{ edit_user_registration_path }']" }

  end

  context 'as an admin' do

    before do
      login_as admin
    end

    it { should have_css ".navbar a[href='#{ sidekiq_web_path }']" }

  end

end
