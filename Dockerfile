FROM devfu/ci:2.3.1

# Copy app
ENV app /src
RUN mkdir $app
WORKDIR $app
ADD . $app

# Run app
CMD rails s -b 0.0.0.0
