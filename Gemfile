source 'https://rubygems.org'

ruby '2.3.1'

gem 'rails', '~> 5.0.0'

# framework dependencies
gem 'coffee-rails', '~> 4.2'
gem 'jbuilder',     '~> 2.5'
gem 'jquery-rails'
gem 'puma',         '~> 3.0'
gem 'sass-rails',   '~> 5.0'
gem 'turbolinks',   '~> 5'
gem 'uglifier',     '>= 1.3.0'

gem 'bootstrap-sass'
gem 'cancancan'
gem 'devise'
gem 'haml-rails'
gem 'hirb'
gem 'omniauth-facebook'
gem 'omniauth-github'
gem 'omniauth-twitter'
gem 'pg'
gem 'pry-rails'
gem 'responders'

# active job queue
gem 'sidekiq'
gem 'rack-protection', git: 'https://github.com/sinatra/rack-protection.git', require: false
gem 'sinatra',         git: 'https://github.com/sinatra/sinatra.git',         require: false

group :production do
  gem 'dalli'
  gem 'memcachier'
  gem 'newrelic_rpm'
  gem 'rails_12factor'
  gem 'unicorn'
end

group :development do
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'dotenv-rails'
  gem 'foreman'
  gem 'pry-byebug'
  gem 'pry-doc'

  gem 'spring'
  gem 'spring-commands-rspec'
  gem 'spring-watcher-listen'
end

group :development, :test do
  gem 'dotenv-rails'
  gem 'faker'
  gem 'rspec-rails'
end

group :test do
  gem 'capybara'
  gem 'codeclimate-test-reporter', require: false
  gem 'database_cleaner'
  gem 'factory_girl_rails'
  gem 'launchy'
  gem 'poltergeist'
  gem 'rspec-its'
  gem 'syntax'
  gem 'timecop'
  gem 'validation_matcher'
end
