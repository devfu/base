Dev Fu! Base
============

A template for creating rails 5.0 applications.

[![build status][ci-image]][ci]

## Development

### Prerequisites

- [docker](https://www.docker.com) Docker CE Edge 17.07+
- [docker-compose](https://docs.docker.com/compose/install)

Prep docker for rails app development (only needed once)

```
$ docker volume create --name=docker-gems
```

Set up the app

```
$ git clone git@gitlab.devfu.com/devfu/base.git
$ cd base
$ docker-compose build
$ docker-compose run bundle install
$ docker-compose run rails db:setup
$ docker-compose run -e RAILS_ENV=test rails db:setup # create test db
$ docker-compose run rails spec
$ docker-compose up -d rails # run rails server in the background
```

- [Web server](http://localhost:30100 'http://localhost:30100')
- [Mailcatcher](http://localhost:30180 'http://localhost:30180')

Environment variables are stored in `.env`, and `.env.#{ RAILS_ENV }`

## Contributing

1. fork
2. test
3. commit
4. merge request
5. repeat

## Deploy

Recommended deployment is via [Heroku](http://heroku.com). They have an excellent intro at http://docs.heroku.com/quickstart

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy)

See `app.json` for heroku app config (addons and environment variables).

## Customization

- Shallow clone (i.e. `git clone --depth 1`)
- s/DevFuBase/YourAppName/g
  - config/application.rb
- s/dev-fu_base/your_app-name/g
  - config/initializers/session_store.rb
- manually review & update
  - .env
  - .env.development
  - .env.test
  - LICENSE.md
  - README.md
  - app.json
  - app/views/welcome/index.html.haml
  - config/initializers/devise.rb
  - config/locales/en.yml

Developed by Dev Fu! LLC

<!-- links -->
[ci]: https://gitlab.devfu.com/devfu/base/builds "build history"

<!-- images -->
[ci-image]: https://gitlab.devfu.com/devfu/base/badges/master/build.svg
