class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  before_action :find_user

  Devise.omniauth_providers.each do |provider|
    define_method provider do
      if @user and @user.persisted?
        sign_in_and_redirect_with_flash_for provider
      else
        session["devise.#{ provider }_data"] = request.env['omniauth.auth']
        redirect_to new_user_registration_url
      end
    end
  end

private

  def find_user
    attrs = request.env['omniauth.auth']
    @user = attrs ? User.from_omniauth(attrs) : nil
  end

  def sign_in_and_redirect_with_flash_for provider
    sign_in_and_redirect @user, :event => :authentication # this will throw if @user is not activated
    set_flash_message :notice, :success, kind: provider.to_s.titleize if is_navigational_format?
  end

end
